import torch.nn as nn
import torch.optim as optim
from torchvision import transforms
from math import floor

import pandas as pd
import os
import numpy as np

import lightning.pytorch as pl
from lightning.pytorch.callbacks.early_stopping import EarlyStopping
from lightning.pytorch.callbacks import ModelCheckpoint

# Other file imports
from utils.postprocessing import postprocess, normalize_image
from dataloader import AODataModule
from utils.unet import *
from utils.metric import closest_pair_removing


class LitTrainAO(pl.LightningModule):
    def __init__(self, model, loss):
        """"
        :param model: The neural network architecture
        :param loss: The loss function
        """
        super(LitTrainAO, self).__init__()
        self.model = model
        self.loss = loss
        self.trainloss = 0

    # Set optimizer with learning rate
    def configure_optimizers(self):
        optimizer = optim.Adam(self.model.parameters(), lr=0.0003)
        return optimizer

    # Define the forward pass through the network
    def forward(self, x):
        return self.model(x)

    # Define the training step
    def training_step(self, batch, batch_idx):
        image, mask, _ = batch
        preds = self.forward(image)
        loss = self.loss(preds, mask.float())
        self.trainloss = loss
        return loss

    # Define the validation step
    def validation_step(self, batch, batch_idx):
        image, mask, validation_mask = batch
        preds = self.forward(image)
        loss = self.loss(preds, mask.float())

        post_mask_zip = self.post_process_images(image, preds, validation_mask)

        mean_metrics = self.calculate_mean_metrics(post_mask_zip)

        self.log_dict(mean_metrics, on_step=False, on_epoch=True)
        self.log_dict({"train_loss": self.trainloss, "val_loss": loss}, on_step=False, on_epoch=True)

        return loss

    def post_process_images(self, image, preds, validation_mask):

        # Get the prediction and images to the CPU
        image_np = image.cpu().numpy()
        preds_np = preds.cpu().numpy()
        validation_mask_np = validation_mask.cpu().numpy()

        # Go from array shape (x, x, 1) to (x, x)
        image_array = np.squeeze(image_np, axis=1)
        pred_array = np.squeeze(preds_np, axis=1)
        mask_array = np.squeeze(validation_mask_np, axis=1)

        norm_pred_array = normalize_image(pred_array)
        norm_mask_array = normalize_image(mask_array)
        norm_image_array = normalize_image(image_array)

        processed_images = postprocess(pred_array, image_array)

        post_mask_zip = zip(norm_image_array, norm_pred_array, processed_images, norm_mask_array)

        return post_mask_zip

    def calculate_mean_metrics(self, post_mask_zip):
        tp_list, pred_cells_list, ann_cells_list, precision_list, recall_list, f1_list = [], [], [], [], [], []

        for image_array, raw_prediction, post_mask, annotation_mask in post_mask_zip:
            metric_dict = closest_pair_removing(pred_mask=post_mask, annotation_mask=annotation_mask, radius=2.4)
            precision_list.append(metric_dict['precision'])
            recall_list.append(metric_dict['recall'])
            f1_list.append(metric_dict['f1_score'])

        mean_precision = np.mean(precision_list)
        mean_recall = np.mean(recall_list)
        mean_f1 = np.mean(f1_list)

        mean_precision = floor((mean_precision) * 10**4) / 10**4
        mean_recall = floor((mean_recall) * 10**4) / 10**4
        mean_f1 = floor((mean_f1) * 10**4) / 10**4

        mean_metrics = {
            'mean_precision': mean_precision,
            'mean_recall': mean_recall,
            'mean_f1': mean_f1
        }

        return mean_metrics

def train_model(model, experiment_dir, experiment_name, max_epochs, patience, number_of_folds):
    fold_scores = []

    for fold in range(number_of_folds):
        model_instance = LitTrainAO(model=model, loss=loss)
        dm_instance = AODataModule(data_dir=data_dir,
                                   batchsize=batchsize,
                                   trans=trans,
                                   num_workers=num_workers,
                                   mask_blur=mask_blur_radius,
                                   folds=number_of_folds,
                                   k=fold)

        early_stopping_callback = EarlyStopping(monitor="mean_f1",
                                                min_delta=0.00,
                                                patience=patience,
                                                verbose=False,
                                                mode="max")

        logger = pl.loggers.CSVLogger(save_dir=experiment_dir, name=f"{experiment_name}", version=f"fold_{fold}")

        checkpoint_callback = ModelCheckpoint(
            monitor='mean_f1',
            filename='{epoch:02d}-{mean_f1:.3f}-{mean_precision:.3f}-{mean_recall:.3f}',
            save_top_k=1,
            mode='max'
        )

        trainer = pl.Trainer(
            deterministic=True,
            logger=logger,
            max_epochs=max_epochs,
            accelerator="auto",
            callbacks=[early_stopping_callback, checkpoint_callback]
        )

        trainer.fit(model_instance, datamodule=dm_instance)

        fold_scores.append({
            'fold': fold,
            'mean_f1': trainer.callback_metrics['mean_f1'].item(),
            'mean_precision': trainer.callback_metrics['mean_precision'].item(),
            'mean_recall': trainer.callback_metrics['mean_recall'].item()
        })

    avg_scores = {
        'mean_f1_avg': sum(score['mean_f1'] for score in fold_scores) / len(fold_scores),
        'mean_precision_avg': sum(score['mean_precision'] for score in fold_scores) / len(fold_scores),
        'mean_recall_avg': sum(score['mean_recall'] for score in fold_scores) / len(fold_scores)
    }

    fold_scores.append(avg_scores)

    scores_df = pd.DataFrame(fold_scores)
    scores_df.to_csv(f'./{experiment_dir}/{experiment_name}/fold_scores.csv', index=False)


if __name__ == "__main__":
    mask_blur_radius = 1.25
    batchsize = 64
    num_workers = 4
    max_epochs = 100
    patience = 25
    data_dir = "data/training dataset"
    image_size = (128, 128)
    number_of_folds = 18
    depth = 32

    experiment_name = f'5-layer-unet_long'
    experiment_dir = f"./training_logs/"
    os.makedirs(experiment_dir, exist_ok=True)

    architecture = UNet_3_layers(1, 1, depth)
    loss = nn.L1Loss()

    pl.seed_everything(42, workers=True)

    trans = transforms.Compose([
        transforms.ToTensor(),
        transforms.CenterCrop(image_size)
    ])

    train_model(architecture, experiment_dir, experiment_name, max_epochs, patience, number_of_folds)

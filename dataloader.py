import torch.utils.data
from torch.utils.data import DataLoader, Dataset, random_split, ConcatDataset
import numpy as np
from PIL import Image, ImageFilter
from torchvision import transforms
from scipy.ndimage import gaussian_filter
import os
from sklearn.model_selection import KFold

import lightning.pytorch as pl
from pathlib import Path

class AOData(Dataset):
    def __init__(self, data_dir="../dataset/", trans=None, mask_blur=0):
        """
        Dataset class for handling AOData images and masks.
        Args:
            data_dir (str): Directory where the dataset is located.
            trans (None): Transformations to be applied to the data.
            mask_blur (int): Degree of mask blur.
        """
        train_dir = Path(data_dir)

        # Get all the images and masks from the directories
        self.images = list(train_dir.glob("images/*.*"))
        self.masks = list(train_dir.glob("masks/*.*"))
        self.trans = trans
        self.mask_blur = mask_blur

    def normalize(self, array):
        """
        Normalize the input array.
        Args:
            array (numpy.array): Input array to be normalized.
        Returns:
            numpy.array: Normalized array.
        """
        min_val = np.min(array)
        max_val = np.max(array)
        normalized_array = ((array - min_val) / (max_val - min_val))
        return normalized_array.astype(np.float32)

    def __getitem__(self, idx):
        """
        Retrieves the image, mask, and the original mask at the given index.
        Args:
            idx (int): Index of the data point.
        Returns:
            tuple: A tuple containing the normalized image, mask, and original mask.
        """
        # Load images and masks
        image = Image.open(self.images[idx].as_posix()).convert('L')
        mask_org = Image.open(self.masks[idx].as_posix()).convert('L')
        mask = gaussian_filter(np.array(mask_org), sigma=self.mask_blur) if self.mask_blur > 0 else np.array(mask_org)

        # Perform transformations
        if self.trans:
            image, mask, mask_org = self.trans(image), self.trans(mask), self.trans(mask_org)

        # Normalize the image and mask for better convergence
        image = self.normalize(np.array(image))
        mask = self.normalize(np.array(mask))

        return image, mask, mask_org

    def __len__(self):
        """
        Returns the length of the dataset.
        """
        return len(self.images)

class AODataModule(pl.LightningDataModule):
    def __init__(self,
                 batchsize: int = 4,
                 data_dir: str = "dataset/",
                 trans = None,
                 num_workers: int = 2,
                 mask_blur: int = 0,
                 folds: int = 5,
                 split_seed: int = 42,
                 k: int = 1
                 ):
        """
        PyTorch Lightning DataModule for handling AOData dataset.
        Args:
            batchsize (int): Batch size for DataLoader.
            data_dir (str): Directory where the dataset is located.
            trans (None): Transformations to be applied to the data.
            num_workers (int): Number of workers for DataLoader.
            mask_blur (int): Degree of mask blur.
            folds (int): Number of folds for K-fold cross-validation.
            split_seed (int): Seed for reproducibility.
            k (int): Index of the fold to use as the validation set.
        """
        super().__init__()

        # Initialize parameters
        self.batchsize = batchsize
        self.data_dir = data_dir
        self.trans = trans
        self.num_workers = num_workers
        self.mask_blur = mask_blur
        self.num_folds = folds
        self.split_seed = split_seed
        self.k = k

        # Initialize data containers
        self.train_data = None
        self.val_data = None

    def setup(self, stage) -> None:
        """
        Setup function for splitting the data into training and validation sets.
        Args:
            stage (str): Stage of training, either "fit" or None.
        """
        if stage == "fit" or stage is None:

            # Get all subfolders and create the splits with the same seed.
            all_subfolders = [os.path.join(self.data_dir, d) for d in os.listdir(self.data_dir)]
            kf = KFold(n_splits=self.num_folds, shuffle=True, random_state=self.split_seed)

            # Get all the subject-indexes from the subfolders
            all_splits = [k for k in kf.split(all_subfolders)]
            train_indexes, validation_indexes = all_splits[self.k]
            train_indexes, validation_indexes = train_indexes.tolist(), validation_indexes.tolist()

            # Get all the training data based on the subject-indexes
            train_data = [AOData(data_dir=all_subfolders[i], trans=self.trans, mask_blur=self.mask_blur) for i in train_indexes]
            val_data = [AOData(data_dir=all_subfolders[i], trans=self.trans, mask_blur=self.mask_blur) for i in validation_indexes]

            # Concatenate the datasets
            self.train_data = ConcatDataset(train_data)
            self.val_data = ConcatDataset(val_data)


    def train_dataloader(self):
        """
        Returns the DataLoader for the training set.
        """
        return DataLoader(self.train_data, batch_size=self.batchsize, num_workers=self.num_workers)

    def val_dataloader(self):
        """
        Returns the DataLoader for the validation set.
        """
        return DataLoader(self.val_data, batch_size=self.batchsize, num_workers=self.num_workers)


# Debuggin test
if __name__ == "__main__":
    trans = transforms.Compose([
        transforms.ToTensor(),
        transforms.CenterCrop((128, 128))
    ])

    dm = AODataModule(data_dir="data/training dataset", trans = trans, batchsize=24)
    dm.setup(stage="fit")

    # Fetching the DataLoader
    train_loader = dm.train_dataloader()

    # Printing the first batch
    for batch_idx, batch in enumerate(train_loader):
        images, masks, mask_orgs = batch  # Assuming the batch consists of images, masks, and original masks
        print(f"Batch {batch_idx}:")
        print("Images:")
        print(images)
        print("Masks:")
        print(masks)
        print("Original Masks:")
        print(mask_orgs)
        break  # Only print the first batch
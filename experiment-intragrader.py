import numpy as np
import os
import imageio
import matplotlib.pyplot as plt
from itertools import combinations


from utils.metric import closest_pair_removing


def find_overlap_area(squares, length):
    # Find and return the coordinates of the overlapping area of the squares
    overlap_coordinates = []
    for i in range(len(squares)):
        for j in range(i + 1, len(squares)):
            x1, y1 = squares[i]
            x2, y2 = squares[j]
            if (x1 <= x2 <= x1 + length or x1 <= x2 + length <= x1 + length) and (
                    y1 <= y2 <= y1 + length or y1 <= y2 + length <= y1 + length):
                x_overlap = max(x1, x2)
                y_overlap = max(y1, y2)
                overlap_coordinates.append(
                    (x_overlap, min(x1 + length, x2 + length), y_overlap, min(y1 + length, y2 + length)))
    return overlap_coordinates


def get_patch_coordinates(txt_path):
    """
    This function retrieves patch coordinates from a text file.

    Args:
    - txt_path (str): Path to the text file containing patch coordinates.

    Returns:
    - patch_coords (list): List of tuples containing the patch coordinates.
    """

    # Create an empty list to store coordinates
    patch_coords = []

    # Read the text file line by line
    with open(txt_path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            data = line.strip().split(',')
            if len(data) < 3:
                raise ValueError("No patches found in the file. Expected format: 'data, x_coord, y_coord'")
            patch_coords.append((int(data[1]), int(data[2])))

    # Return the patch coordinates list
    return patch_coords


def get_intragrader_score(data_path, eccentricity_range, radius):

    intra_grader_scores = {}
    inter_grader_scores = {}

    # Iterate through subdirectories
    for subdir in os.listdir(data_path):

        # Split the filename on underscores
        parts = subdir.split('_')
        x_value = float(parts[2][1:4]) * (-1 if parts[2][-1] != "T" else 1)

        # Check if the value is in the eccentricity range
        if x_value < eccentricity_range[0] or x_value > eccentricity_range[1]:
            continue

        # Check if subdirectory actually exists
        subdir_path = os.path.join(data_path, subdir)
        if os.path.isdir(subdir_path) == False:
            continue

        # Construct file paths for annotation graders and other relevant files
        grader_paths = [os.path.join(subdir_path, f"annotation_grader_{i}.tiff") for i in range(1, 5)]
        ao_detect_path = os.path.join(subdir_path, "annotation_AO_detect.tiff")
        patch_position_path = os.path.join(subdir_path, "patch_positions.txt")

        # Obtain patch coordinates and overlap areas from the text file for intragrader
        patches_coordinates = get_patch_coordinates(txt_path=patch_position_path)
        total_overlap_areas = find_overlap_area(patches_coordinates, 128)

        # Read and convert TIFF annotations to NumPy arrays
        grader_arrays = [np.array(imageio.mimread(path)) for path in grader_paths]
        #grader_arrays.append(np.array(imageio.mimread(ao_detect_path)))

        """
        Intra-grader loop
        """
        for i, overlap_area in enumerate(total_overlap_areas):


            overlap_x_start, overlap_x_end, overlap_y_start, overlap_y_end = overlap_area

            patch_1_x_start = overlap_x_start - patches_coordinates[i][0]
            patch_1_x_end = overlap_x_end - patches_coordinates[i][0]
            patch_1_y_start = overlap_y_start - patches_coordinates[i][1]
            patch_1_y_end = overlap_y_end - patches_coordinates[i][1]

            patch_2_x_start = overlap_x_start - patches_coordinates[i + 1][0]
            patch_2_x_end = overlap_x_end - patches_coordinates[i + 1][0]
            patch_2_y_start = overlap_y_start - patches_coordinates[i + 1][1]
            patch_2_y_end = overlap_y_end - patches_coordinates[i + 1][1]


            for x, annotation_array in enumerate(grader_arrays):

                overlap_zone_1 = annotation_array[i][patch_1_y_start: patch_1_y_end, patch_1_x_start: patch_1_x_end]
                overlap_zone_2 = annotation_array[i+1][patch_2_y_start: patch_2_y_end, patch_2_x_start: patch_2_x_end]

                score_dict = closest_pair_removing(overlap_zone_1, overlap_zone_2, radius=radius)

                if x + 1 not in intra_grader_scores:
                    intra_grader_scores[x + 1] = []

                intra_grader_scores[x+1].append(score_dict["f1_score"])



    return intra_grader_scores




if __name__ == "__main__":


    data_path_test_set = "data/test dataset"

    image_patchsize = 128
    patch_amount = 3

    eccenctricity_ranges = [(-2,2),(3,6),(7,10)]

    ecc_scores = {}

    for ecc in eccenctricity_ranges:

        print("")
        print(f"Eccentricity range: {ecc}")
        scores = get_intragrader_score(data_path=data_path_test_set, eccentricity_range=ecc, radius = 2.4)
        ecc_scores[ecc] = scores

        all_scores = []

        for grader, grader_scores in scores.items():

            score = np.mean(grader_scores)
            std = np.std(grader_scores)

            print(f"Grader {grader}: " + "{:.2f}".format(score) + " ± " + "{:.2f}".format(std))




        #     all_scores += grader_scores
        #     # Sem or Me
        #     if grader == 1 or grader == 3:
        #         score = np.mean(scores[1] + scores[3])
        #         std = np.std(scores[1] + scores[3])
        #
        #         print(f"Grader {grader}: " + "{:.2f}".format(score) + " ± " + "{:.2f}".format(std))
        #
        #     if grader == 2 or grader == 4:
        #         score = np.mean(grader_scores)
        #         std = np.std(grader_scores)
        #
        #         print(f"Grader {grader}: " + "{:.2f}".format(score) + " ± " + "{:.2f}".format(std))
        #
        # print("{:.2f}".format(np.mean(all_scores)) + " ± " + "{:.2f}".format(np.std(all_scores)))
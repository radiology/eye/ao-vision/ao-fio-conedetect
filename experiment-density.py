import numpy as np
import os
from predict import ImagePredictor
import imageio
import matplotlib.pyplot as plt
import math

# import seaborn as sns
import pandas as pd
plt.rcParams.update({'font.size': 20})

from utils.metric import closest_pair_removing
from utils.unet import *
import matplotlib
matplotlib.use('TkAgg')
import csv

def get_age(df, subject_name):
    return df.loc[subject_name, 'Age']


def calc_array_density_stack(array_stack, cut_off):
    density_list = []
    len_density_list = int(math.sqrt(len(array_stack)))

    for idx, array in enumerate(array_stack):
        row_idx = idx // len_density_list
        col_idx = idx % len_density_list

        if cut_off <= row_idx <= (len_density_list - cut_off) and cut_off <= col_idx <= (len_density_list - cut_off):
            density_list.append(calc_array_density_patch(array))

    return np.median(density_list)

def calc_array_density_patch(array):
    # Count non-zero values
    non_zero_count = np.count_nonzero(array)

    # Calculate the area of the cropped array in square millimeters
    array_area_mm2 = (98 * 98) / (1000 ** 2)  # Convert microns to millimeters

    # Calculate cell density (cells/mm²)
    cell_density = non_zero_count / array_area_mm2

    return cell_density


def get_density_scores_full_set(data_path, selected_data_path, excel_file_path, predictor, eccentricity_range):
    total_scores_dict = {}


    # Set 'Subjectnumber' as the index
    df = pd.read_excel(excel_file_path)
    df.set_index('Subjectnumber', inplace=True)


    total = 0

    for subject in os.listdir(data_path):
        subject_path = os.path.join(data_path, subject)
        selected_subject_path = os.path.join(selected_data_path, subject)

        try:
            subject_age = get_age(df=df, subject_name=subject)
        except:
            continue


        if not os.path.exists(selected_subject_path):
            continue

        array_file_paths = [os.path.basename(array_path)[:-4] for array_path in os.listdir(subject_path)]
        selected_image_paths = [os.path.basename(selected_image)[:-4] for selected_image in
                                os.listdir(selected_subject_path)]

        # Convert the lists to sets and find the intersection
        common_paths = set(array_file_paths).intersection(selected_image_paths)

        # Convert the result back to a list if needed
        common_paths_list = list(common_paths)

        # Create eccentricity list
        eccentricity_base_path_list = []
        x_values = []

        for common_path in common_paths_list:
            parts = common_path.split('_')
            x_value = float(parts[2][1:4]) * (-1 if parts[2][-1] != "T" else 1)

            if eccentricity_range[0] <= x_value <= eccentricity_range[1]:
                eccentricity_base_path_list.append(common_path)
                x_values.append(x_value)

        # Create the actual full paths
        full_array_paths = [os.path.join(data_path, subject, basename + ".npy") for basename in
                            eccentricity_base_path_list]

        arrays = [np.load(full_array_path) for full_array_path in full_array_paths]
        densities_2 = [calc_array_density_stack(array_stack, 2) for array_stack in arrays]
        densities_4 = [calc_array_density_stack(array_stack, 4) for array_stack in arrays]
        densities_6 = [calc_array_density_stack(array_stack, 6) for array_stack in arrays]
        densities_full_image = [calc_array_density_stack(array_stack, 0) for array_stack in arrays]


        # density_zip = zip(densities_2, densities_4, densities_6, densities_full_image)
        #
        # for x, (density_2, density_4, density_6, density_full_image) in enumerate(density_zip):
        #     x_value = x_values[x]
        #
        #     if x_value not in total_scores_dict:
        #         total_scores_dict[x_value] = {"AO-FIO ConeDetect: Full image": [],
        #                                       "AO-FIO ConeDetect: 36 patches": [],
        #                                         "AO-FIO ConeDetect: 4 patches": []}
        #
        #     total_scores_dict[x_value]["AO-FIO ConeDetect: Full image"].append(density_full_image)
        #     total_scores_dict[x_value]["AO-FIO ConeDetect: 36 patches"].append(density_4)
        #     total_scores_dict[x_value]["AO-FIO ConeDetect: 4 patches"].append(density_6)

        for x, density in enumerate(densities_4):
            x_value = x_values[x]

            if x_value not in total_scores_dict:
                total_scores_dict[x_value] = {"AO-FIO ConeDetect: Age 18-30": [],
                                              "AO-FIO ConeDetect: Age 50+": []}

            if int(subject_age) < 30:
                total_scores_dict[x_value]["AO-FIO ConeDetect: Age 18-30"].append(density)


            if int(subject_age) > 50:
                total_scores_dict[x_value]["AO-FIO ConeDetect: Age 50+"].append(density)

    return total_scores_dict


def get_density_scores_testset(data_path, predictor, eccentricity_range):
    total_scores_dict = {}

    # Iterate through subdirectories
    for subdir in os.listdir(data_path):

        # Split the filename on underscores
        parts = subdir.split('_')
        x_value = float(parts[2][1:4]) * (-1 if parts[2][-1] != "T" else 1)

        # Check if the value is in the eccentricity range
        if x_value < eccentricity_range[0] or x_value > eccentricity_range[1]:
            continue

        # Check if subdirectory actually exists
        subdir_path = os.path.join(data_path, subdir)
        if not os.path.isdir(subdir_path):
            continue

        # Get all paths
        grader_paths = [os.path.join(subdir_path, f"annotation_grader_{i}.tiff") for i in range(1, 5)]
        ao_detect_path = os.path.join(subdir_path, "annotation_AO_detect.tiff")
        aggregate_path = os.path.join(subdir_path, "aggretate_annotation.tiff")
        image_patches_name = [path for path in os.listdir(subdir_path) if os.path.basename(path).startswith("patches")]
        image_patches_path = os.path.join(subdir_path, image_patches_name[0])

        # Load all tiff images as arrays
        grader_arrays = [np.array(imageio.mimread(path)) for path in grader_paths]
        ao_detect_array = np.array(imageio.mimread(ao_detect_path))
        image_patches = np.array(imageio.mimread(image_patches_path))
        aggregate_patches = np.array(imageio.mimread(aggregate_path))

        # Create the model predictions of the images
        model_prediction = predictor.predict_numpy_array(numpy_array=image_patches)

        # Iterate over all patches in the TIFF file
        for patch_idx, model_patch in enumerate(model_prediction):

            ao_detect_density = calc_array_density_patch(ao_detect_array[patch_idx])
            model_density = calc_array_density_patch(model_patch)
            aggregate_density = calc_array_density_patch(aggregate_patches[patch_idx])

            score_dict = {"AO-detect": ao_detect_density,
                          "AO-FIO ConeDetect": model_density
                          }

            """
            Center-based
            """
            # center_1_density = (calc_array_density_patch(grader_arrays[0][patch_idx]) + calc_array_density_patch(
            #     grader_arrays[2][patch_idx])) / 2
            # center_2_density = calc_array_density_patch(grader_arrays[1][patch_idx])
            # center_3_density = calc_array_density_patch(grader_arrays[2][patch_idx])
            #
            # score_dict["Center 1"] = center_1_density
            # score_dict["Center 2"] = center_2_density
            # score_dict["Center 3"] = center_3_density
            #
            # store_val = ""
            #
            # if -2 <= x_value <= 2:
            #     store_val = "2°N - 2°T"
            #
            # if 3 <= x_value <= 6:
            #     store_val = "3°T - 6°T"
            #
            # if 7 <= x_value <= 10:
            #     store_val = "7°T - 10°T"
            #
            # if store_val not in total_scores_dict:
            #     total_scores_dict[store_val] = []
            #
            # total_scores_dict[store_val].append(score_dict)

            """
            Grader based
            """
            for grader, grader_array in enumerate(grader_arrays):
                grader_density = calc_array_density_patch(grader_array[patch_idx])
                grader_name = f"Grader {grader + 1}"
                score_dict[grader_name] = grader_density

            if x_value not in total_scores_dict:
                total_scores_dict[x_value] = []

            total_scores_dict[x_value].append(score_dict)

    return total_scores_dict


def plot_testset_densities(total_scores_dict, score_dict = "testset"):
    Muthiah_manual = {2: 26500, 3: 19500, 5: 13800, 7: 11200}
    Muthiah_manual_label = "Manual Muthiah et al. 2014"

    Muthiah_automated = {2: 24200, 3: 20800, 5: 15600, 7: 12900}
    Muthiah_automated_label = "Automatic Muthiah et al. 2014"

    Legras = {2: 31437, 3: 24747, 4: 20072, 5: 17734}
    Legras_label = "Legras et al. 2018"

    sorted_dict = dict(sorted(total_scores_dict.items()))

    x_values = list(sorted_dict.keys())

    plt.figure(figsize=(12, 9))
    plt.gca().tick_params(axis='x', pad=15)

    # average_density_grader = []
    # std_density_grader = []
    #
    # for x_value in x_values:
    #     scores_array = sorted_dict[x_value]
    #     center_1_list = [score_dict["Center 1"] for score_dict in scores_array]
    #     center_2_list = [score_dict["Center 2"] for score_dict in scores_array]
    #     center_3_list = [score_dict["Center 3"] for score_dict in scores_array]
    #     total_list = center_1_list + center_2_list + center_3_list
    #
    #     median, std = np.median(total_list), np.std(total_list)
    #     average_density_grader.append(median)
    #     std_density_grader.append(std)
    #
    # print(
    #     f"Density avg graders: 2N-2T: ({int(average_density_grader[0])} ± {int(std_density_grader[0])}), 3T-6T: ({int(average_density_grader[1])} ± {int(std_density_grader[1])}), 7T-10T: ({int(average_density_grader[2])} ± {int(std_density_grader[2])})")

    # Model plot
    colors = ["#99CC00", "#FFCC00", "#66B2FF", "#3399FF", "#0066CC", "#99CCFF", "#000000"]

    # # Create a CSV file for writing
    # with open('model_data.csv', mode='w', newline='') as csv_file:
    #     csv_writer = csv.writer(csv_file)
    #
    #     # Write the header row with the keys
    #     keys = sorted(sorted_dict[x_values[0]][0].keys())
    #     csv_writer.writerow(['X Value'] + keys)
    #
    #     for x_value in x_values:
    #         median_density = []
    #         average_std = []
    #         row_data = []
    #
    #         for x, key in enumerate(keys):
    #             scores_array = sorted_dict[x_value]
    #             density_list = [score_dict[key] for score_dict in scores_array]
    #             median, std = np.median(density_list), np.std(density_list)
    #             median_density.append(median)
    #             average_std.append(std)
    #             row_data.append(f"{int(median)} ± {int(std)}")
    #
    #         csv_writer.writerow([f"{x_value}"] + row_data)
    #         print(f"{x_value}: {', '.join(row_data)}")

    # Create a CSV file for writing
    with open('model_data_transposed.csv', mode='w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)

        # Write the header row with the x_values
        csv_writer.writerow(['Key'] + [f"{x_value}" for x_value in x_values])

        # Get the keys
        keys = sorted(sorted_dict[x_values[0]][0].keys())

        for x, key in enumerate(keys):
            median_density = []
            average_std = []
            row_data = []

            for x_value in x_values:
                scores_array = sorted_dict[x_value]
                density_list = [score_dict[key] for score_dict in scores_array]
                median, std = np.median(density_list), np.std(density_list)
                median_density.append(median)
                average_std.append(std)
                row_data.append(f"{int(median)} ± {int(std)}")

            csv_writer.writerow([key] + row_data)
            print(f"{key}: {', '.join(row_data)}")

    for x, key in enumerate(sorted_dict[x_values[0]][0].keys()):
        median_density = []
        average_std = []

        for x_value in x_values:
            scores_array = sorted_dict[x_value]
            density_list = [score_dict[key] for score_dict in scores_array]
            median, std = np.median(density_list), np.std(density_list)
            median_density.append(median)
            average_std.append(std)

            print(f"{key}, Xvalue{x_value}: {int(median)} ± {int(std)}")

        fmt = 'o--'
        width = 1

        if key == "AO-detect" or key == "Proposed algorithm":
            fmt = "o-"
            width = 2

        plt.errorbar(x_values, median_density, yerr=average_std, fmt=fmt, label=key, linewidth=width, color=colors[x])

    # Plot additional data points
    plt.plot(list(Muthiah_manual.keys()), list(Muthiah_manual.values()), 's-', label=Muthiah_manual_label, color="#f6ee00")
    plt.plot(list(Muthiah_automated.keys()), list(Muthiah_automated.values()), 's-', label=Muthiah_automated_label, color="#f65f2f")
    plt.plot(list(Legras.keys()), list(Legras.values()), 's-', label=Legras_label, color="#d60000")

    plt.xlabel('Eccentricity (degrees)')
    plt.ylabel('Median Cell Density (cells/mm²)')
    # ax.yaxis.labelpad = 20  # You can adjust this value as needed
    plt.ylim(5000, 35000)
    plt.legend()
    plt.savefig("test_density.pdf")




def plot_fullset_densities(total_scores_dict):
    Muthiah_manual = {2: 26500, 3: 19500, 5: 13800, 7: 11200}
    Muthiah_manual_label = "Manual Muthiah et al. 2014"

    Muthiah_automated = {2: 24200, 3: 20800, 5: 15600, 7: 12900}
    Muthiah_automated_label = "Automatic Muthiah et al. 2014"

    Legras = {2: 31437, 3: 24747, 4: 20072, 5: 17734}
    Legras_label = "Legras et al. 2018"

    sorted_dict = dict(sorted(total_scores_dict.items()))


    x_values = list(sorted_dict.keys())

    value = sorted_dict[x_values[0]]

    plt.figure(figsize=(12, 9))
    plt.gca().tick_params(axis='x', pad=15)

    print(sorted_dict[x_values[0]].keys())

    # Model plot
    colors = ["#6bbd32", "#0a4cd8", "#66B2FF", "#3399FF", "#0066CC", "#99CCFF"]


    for k, key in enumerate(sorted_dict[x_values[0]].keys()):
        median_density = []
        average_std = []

        for x_value in x_values:
            scores_array = sorted_dict[x_value]
            density_list = scores_array[key]

            median, std = np.median(density_list), np.std(density_list)
            median_density.append(median)
            average_std.append(std)


        fmt = 'o-'
        width = 2


        plt.errorbar(x_values, median_density, yerr=average_std, fmt=fmt, label=key, linewidth=width, color=colors[k])

    # Plot additional data points
    plt.plot(list(Muthiah_manual.keys()), list(Muthiah_manual.values()), 's-', label=Muthiah_manual_label, color="#f6ee00")
    plt.plot(list(Muthiah_automated.keys()), list(Muthiah_automated.values()), 's-', label=Muthiah_automated_label, color="#f65f2f")
    plt.plot(list(Legras.keys()), list(Legras.values()), 's-', label=Legras_label, color="#d60000")

    plt.xlabel('Eccentricity (degrees)')
    plt.ylabel('Median Cell Density (cells/mm²)')
    plt.ylim(5000, 35000)
    plt.legend()
    plt.savefig("full_density_age.pdf")

if __name__ == "__main__":
    test_dataset_path = "data/test dataset"

    ages_path = "data/Ages_and_sex_AOVH.xlsx"

    architecture = UNet_5_layers(1, 1, 32)
    checkpoint_path = "utils/model/epoch=47-mean_f1=0.935-mean_precision=0.933-mean_recall=0.939.ckpt"

    predictor = ImagePredictor(architecture=architecture, checkpoint_path=checkpoint_path)



    """
    Test dataset density experiment
    """
    density_score_dict = get_density_scores_testset(data_path=test_dataset_path,
                                                    predictor=predictor,
                                                    eccentricity_range=(2, 10))
    plot_testset_densities(density_score_dict)

    """
    Full dataset density experiment with ages.
    """

    density_score_dict2 = get_density_scores_full_set(data_path="Path",
                                                      selected_data_path="Path",
                                                      predictor=predictor,
                                                      excel_file_path=ages_path,
                                                      eccentricity_range=(2, 10))

    plot_fullset_densities(density_score_dict2)

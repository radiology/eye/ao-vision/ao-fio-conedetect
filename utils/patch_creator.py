import numpy as np
from PIL import Image

class ImageSlicer:

    def normalize_image(self, image: np.ndarray, is_mask: bool = False) -> np.ndarray:
        """
        Normalize the pixel values of the given image to the range [0, 255].

        :param image: Input image (numpy array).
        :param is_mask: If True, scales the values to [0, 255]. If False, scales the values to [0, 1].
        :return: Normalized image (numpy array).
        """
        if image.size == 0:
            raise ValueError("Input image is empty.")

        min_val = np.min(image)
        max_val = np.max(image)

        if is_mask:
            normalized_image = (255 * ((image - min_val) / (max_val - min_val))).astype(np.uint8)
        else:
            normalized_image = ((image - min_val) / (max_val - min_val))

        return normalized_image

    def calculate_start_points(self, size, subframe_size, overlap=None):
        """
        Generate a list of starting points for splitting a given size into segments.

        Parameters:
            size (int): The total size to be split.
            subframe_size (int): The size of each segment.
            overlap (float or int, optional): The overlap amount or percentage between segments.
                - If None or 0, no overlap is used.
                - If float (0 to 1), it represents the overlap as a percentage of split_size.
                - If int (> 1), it represents the overlap as a fixed pixel amount.

        Returns:
            list: A list of starting points for splitting the size.

        Example:
            start_points(10, 3, overlap=0.2) => [0, 2, 4, 6]
        """
        points = []

        if overlap is None or overlap == 0:
            stride = int(subframe_size)
        elif isinstance(overlap, float) and 0 < overlap <= 1:
            stride = int(subframe_size * (1 - overlap))
        elif isinstance(overlap, int) and overlap > 1:
            stride = int(subframe_size - overlap)
        else:
            raise ValueError("Overlap must be a float between 0 and 1 or an integer greater than 1.")

        for pt in range(0, size, stride):
            if pt + subframe_size >= size:
                points.append(size - subframe_size)
                if points[-1] == size - subframe_size:
                    break
            else:
                points.append(pt)

        return points



    def create_subframes(self, image_array, subframe_size, overlap, exclude_edge=2):
        """
        Splits an input image into overlapping subframes of a specified size.

        Parameters:
        - image_array (numpy.ndarray): The input image as a numpy array.
        - subframe_size (int): The size of each subframe (both width and height).
        - overlap (int): The number of pixels for overlap between subframes.

        Returns:
        - subframes (list of numpy.ndarray): List of subframes extracted from the input
         image with shape (number of subframes, subframe height, subframe_width).
        - y_startpoints (list of int): List of Y-axis starting points for subframes.
        - x_startpoints (list of int): List of X-axis starting points for subframes.
        """

        image_array = self.normalize_image(image_array)
        image_width, image_height = np.shape(image_array)

        y_startpoints = self.calculate_start_points(image_width, subframe_size, overlap)
        x_startpoints = self.calculate_start_points(image_height, subframe_size, overlap)

        subframes = []

        for count_i, i in enumerate(y_startpoints):
            for count_j, j in enumerate(x_startpoints):

                if (count_i < exclude_edge or count_j < exclude_edge) and exclude_edge > 0:
                    continue

                if (count_i > len(x_startpoints) - exclude_edge - 1
                    or count_j > len(y_startpoints) - exclude_edge - 1) and  exclude_edge > 0:
                    continue

                subframes.append(image_array[i:i + subframe_size, j:j + subframe_size])

        return subframes, x_startpoints, y_startpoints

    def reconstruct_image(self, image_shape, subframes, x_startpoints, y_startpoints, exclude_edge=0):
        """
        Reconstructs an image from a list of overlapping subframes with exclusion.

        Parameters:
        - image_shape (tuple): The shape (height, width) of the output image.
        - subframes (list of numpy.ndarray): List of subframes to be used for reconstruction.
        - y_startpoints (list of int): List of Y-axis starting points for subframes.
        - x_startpoints (list of int): List of X-axis starting points for subframes.
        - exclude_edge (int): Number of subframes to exclude from the edges.

        Returns:
        - reconstructed_img (numpy.ndarray): The reconstructed image as a numpy array.
        """

        reconstructed_img = np.zeros(image_shape)
        subframe_idx = 0

        for i, y_start in enumerate(y_startpoints):
            for j, x_start in enumerate(x_startpoints):

                if (i < exclude_edge or j < exclude_edge) and exclude_edge > 0:
                    continue

                if (i > len(x_startpoints) -  1 or j > len(y_startpoints) - 1) and exclude_edge > 0:
                    continue

                subframe = subframes[subframe_idx]
                reconstructed_img[y_start:y_start + subframe.shape[0], x_start:x_start + subframe.shape[1]] = subframe
                subframe_idx += 1

        # Normalize reconstructed array for visualization
        reconstructed_img = self.normalize_image(reconstructed_img, True)

        return reconstructed_img

    # def patch_montage(self, image_montage):



if __name__ == "__main__":

    # Parameters for patch size, overlap, and exclude edge
    PATCH_SIZE = 128
    OVERLAP = 20
    EXCLUDE_EDGE = 4

    # Image patch for testing
    path_to_img = "../data/exampleimages/exampleRTX_image.png"

    # Initialize slicer
    slicer = ImageSlicer()

    # Load image using Pillow and transform to grayscale
    img = np.array(Image.open(path_to_img).convert("L"))
    Image.fromarray(slicer.normalize_image(img, True))

    # Get image shape
    image_shape = np.shape(img)

    print(image_shape)

    # Create subframes
    subframes, x_points, y_points = slicer.create_subframes(image_array=img,
                                                            subframe_size=PATCH_SIZE,
                                                            overlap=OVERLAP,
                                                            exclude_edge=EXCLUDE_EDGE)\


    print(np.shape(subframes))

    print(x_points, y_points)

    # Print the frames shapes array
    print(f"Number of subframes created: {np.shape(subframes)[0]}")

    # Reconstruct the image
    recon_image = slicer.reconstruct_image(image_shape=image_shape, subframes=subframes, x_startpoints=x_points,
                                           y_startpoints=y_points, exclude_edge=EXCLUDE_EDGE)

    # Check if the images are the same
    if np.array_equal(img, recon_image):
        print("Image correctly reconstructed - All values the same")

    # Plot the reconstructed array
    Image.fromarray(slicer.normalize_image(recon_image, True)).show()

import numpy as np
import cv2
from PIL import Image


def normalize_image(image: np.ndarray, is_mask: bool = False) -> np.ndarray:
    """
    Normalize the pixel values of the given image to the range [0, 255].

    :param image: Input image (numpy array).
    :param is_mask: If True, scales the values to [0, 255]. If False, scales the values to [0, 1].
    :return: Normalized image (numpy array).
    """
    min_val = np.min(image)
    max_val = np.max(image)

    if is_mask:
        normalized_image = (255 * ((image - min_val) / (max_val - min_val))).astype(np.uint8)
    else:
        normalized_image = ((image - min_val) / (max_val - min_val)).astype(np.float32)

    return normalized_image

def postprocess(pred_array: np.ndarray, image_array: np.ndarray) -> np.ndarray:
    """
    Perform post-processing on the predicted array using the corresponding image array.
    Returns a stack of processed images.

    Steps of the postprocessing pipeline:
    1. Normalize image mask to values between 0 and 255
    2. Otsu thresholding on each raw prediction
    3. Connected components on each binary image
    4. Obtain maximum intensity value of the connected components area in the original image
    5. Convert max intensity value to center coordinates.

    :param pred_array: Predicted array (numpy array) with shape (image number, image height, image width).
    :param image_array: Corresponding image array for post-processing (numpy array) with shape (image number, image height, image width)..
    :return: Stack of processed images (numpy array) with shape (image number, image height, image width)..
    """
    processed_images = []

    if np.shape(pred_array) != np.shape(image_array):
        raise ValueError(f"Image shapes of prediction array ({np.shape(pred_array)}) and image array ({np.shape(image_array)}) are not the same size.")

    if len(np.shape(pred_array)) != 3 or len(np.shape(image_array)) != 3:
        raise ValueError(f"Prediction array or image array has the wrong shape. It needs to be (x, 128, 128), currently is: prediction array ({np.shape(pred_array)}) and image array ({np.shape(image_array)})")

    # Normalize prediction array between 0 and 255 for CV2 threshold
    normalized_pred_array = normalize_image(pred_array, is_mask=True)

    for i, prediction_array in enumerate(normalized_pred_array):
        _, thresholded_image = cv2.threshold(prediction_array, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        # Find connected components in the thresholded image
        num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(thresholded_image)

        # Filter small areas
        large_components = stats[1:, cv2.CC_STAT_AREA] >= 2

        # Get the coordinates of the components
        coords = stats[1:][large_components, :4]

        # Extract the regions of interest (ROIs) from the original image
        rois = [image_array[i][y:y + h, x:x + w] for x, y, w, h in coords]

        max_intensity_locations = []

        for roi in rois:
            amount_of_maxes = len(np.argwhere(roi == np.max(roi)))

            # Only one maximum intensity peak
            if amount_of_maxes == 1:
                max_intensity_locations.append(np.unravel_index(np.argmax(roi), roi.shape))

            # If there are multiple intensity points
            if amount_of_maxes > 1:
                roi_max_intensity_locations = np.argwhere(roi == np.max(roi))

                # Find the index that is closest to the center of the ROI
                center = np.array(roi.shape) // 2
                closest_index = min(roi_max_intensity_locations, key=lambda x: np.linalg.norm(center - x))
                max_intensity_locations.append(tuple(closest_index))

        # Convert local coordinates to global coordinates
        max_intensity_coordinates = [(loc[0] + coord[1], loc[1] + coord[0]) for loc, coord in zip(max_intensity_locations, coords)]

        # Create a blank image of the same size as the original image
        highlighted_image = np.zeros_like(image_array[i], dtype=np.uint8)

        # Set the intensity values at the maximum intensity points to a high value (e.g., 255)
        for point in max_intensity_coordinates:
            highlighted_image[point[0], point[1]] = 1

        processed_images.append(highlighted_image)

    return np.stack(processed_images, axis=0)


if __name__ == "__main__":
    npy_file = "C:/Users/Sander/PycharmProjects/AOSEG/ConeSeg/raw_pred.npy"
    raw_array = np.load(npy_file)
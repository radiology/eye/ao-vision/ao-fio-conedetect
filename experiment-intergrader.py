import numpy as np
from itertools import product
import os
import imageio
from utils.metric import closest_pair_removing
import matplotlib.pyplot as plt

def get_intergrader_score(data_path, eccentricity_range, radius):

    """
    The experiment to obtain the intergrader score between the four graders.

    :param data_path: The path to the test dataset, type: str
    :param eccentricity_range: The range in which to look for the intergrader score, type tuple (int, int)
    :param radius: Metric radius in pixels, type: float

    :return: Returns a metric dictionary with the following structure:
    {"1 vs 2": [value, value, value, ...],
    "1 vs 3": [value, value, value, ...],
    .....,
    "3 vs 4": [value, value, value]}

    """

    total_scores = {}

    # Iterate through subdirectories
    for subdir in os.listdir(data_path):

        # Split the filename on underscores
        parts = subdir.split('_')
        x_value = float(parts[2][1:4]) * (-1 if parts[2][-1] != "T" else 1)

        # Check if the value is in the eccentricity range
        if x_value < eccentricity_range[0] or x_value > eccentricity_range[1]:
            continue

        # Check if subdirectory actually exists
        subdir_path = os.path.join(data_path, subdir)
        if os.path.isdir(subdir_path) == False:
            continue

        # Construct file paths for annotation graders and other relevant files
        grader_paths = [os.path.join(subdir_path, f"annotation_grader_{i}.tiff") for i in range(1, 5)]
        ao_detect_path = os.path.join(subdir_path, "annotation_AO_detect")
        patch_position_path = os.path.join(subdir_path, "patch_positions.txt")

        # Read and convert TIFF annotations to NumPy arrays
        grader_arrays = [np.array(imageio.mimread(path)) for path in grader_paths]


        # Get unique intergrader scores for pairs
        unique_combinations = []
        for i in range(len(grader_arrays)):
            for j in range(i + 1, len(grader_arrays)):

                combo = (grader_arrays[i], grader_arrays[j])

                # Ensure the combination is not symmetrical
                if not any(np.array_equal(combo[0], arr) and np.array_equal(combo[1], arr) for arr in unique_combinations):

                    for patch_idx in range(np.shape(grader_arrays[i])[0]):

                        patch_i = grader_arrays[i][patch_idx]
                        patch_j = grader_arrays[j][patch_idx]

                        score = closest_pair_removing(pred_mask=patch_i,
                                                      annotation_mask=patch_j,
                                                      radius=radius)

                        inv_score = closest_pair_removing(pred_mask=patch_j,
                                                          annotation_mask=patch_i,
                                                          radius=radius)

                        avg_score = (score["f1_score"] + inv_score["f1_score"]) / 2

                        comb_string = f"{i + 1} vs {j + 1}"

                        if comb_string not in total_scores:
                            total_scores[comb_string] = []

                        total_scores[comb_string].append(avg_score)

    return total_scores




if __name__ == "__main__":

    data_path_test_set = "data/test dataset"

    eccenctricity_ranges = [(-2,2),(3,6),(7,10)]

    ecc_scores = {}

    for ecc in eccenctricity_ranges:

        print("")
        print(f"Eccentricity range: {ecc}")
        scores = get_intergrader_score(data_path=data_path_test_set, eccentricity_range=ecc, radius = 2.4)
        ecc_scores[ecc] = scores

        for grader, grader_scores in scores.items():

            score = np.mean(grader_scores)
            std = np.std(grader_scores)

            print(f"{grader}: " + "{:.2f}".format(score) + " ± " + "{:.2f}".format(std))


        # combined_1_2 = []
        # combined_1_3 = []
        # combined_2_3 = []
        #
        # for grader, grader_scores in scores.items():
        #
        #     if grader == "1 vs 2" or grader == "2 vs 3":
        #         combined_1_2 += grader_scores
        #
        #     if grader == "1 vs 4" or grader == "3 vs 4":
        #         combined_1_3 += grader_scores
        #
        #     if grader == "2 vs 4":
        #         combined_2_3 += grader_scores
        #
        #     score = np.mean(grader_scores)
        #     std = np.std(grader_scores)
        #
        #
        #
        # print(f"Center 1 VS 2: " + "{:.2f}".format(np.mean(combined_1_2)) + " ± " + "{:.2f}".format(np.std(combined_1_2)))
        # print(f"Center 1 VS 3: " + "{:.2f}".format(np.mean(combined_1_3)) + " ± " + "{:.2f}".format(np.std(combined_1_3)))
        # print(f"Center 2 VS 3: " + "{:.2f}".format(np.mean(combined_2_3)) + " ± " + "{:.2f}".format(np.std(combined_2_3)))
        #
        # print(f"TOTAL" + "{:.2f}".format(np.mean(combined_1_2 + combined_1_3 + combined_2_3)) +  " ± " + "{:.2f}".format(np.std(combined_1_2 + combined_1_3 + combined_2_3)))
        #
